package it.hackubau.saleTaxes;

import it.hackubau.saleTaxes.services.TaxesService;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TaxesServiceTest {

    @Autowired
    TaxesService taxesService;


    @Test
    public void t01_taxAmount() {
        BigDecimal zero = new BigDecimal(0);
        BigDecimal uno = new BigDecimal(1);
        BigDecimal dieci = new BigDecimal(10);
        BigDecimal cento = new BigDecimal(100);
        BigDecimal meno5 = new BigDecimal(-5);


        Assert.assertEquals(taxesService.getTaxAmount(zero, zero).compareTo(zero), 0);
        Assert.assertEquals(taxesService.getTaxAmount(zero, uno).compareTo(zero), 0);
        Assert.assertEquals(taxesService.getTaxAmount(uno, zero).compareTo(zero), 0);
        Assert.assertEquals(taxesService.getTaxAmount(uno, cento).compareTo(uno), 0);
        Assert.assertEquals(taxesService.getTaxAmount(dieci, zero).compareTo(zero), 0);
        Assert.assertEquals(taxesService.getTaxAmount(dieci, dieci).compareTo(uno), 0);
        Assert.assertEquals(taxesService.getTaxAmount(dieci, meno5).compareTo(new BigDecimal("-0.5")), 0);


        Assert.assertEquals(taxesService.getTaxedPrice(dieci, meno5).compareTo(new BigDecimal("9.5")), 0);

        //TODO test all cases and methods
    }


}
