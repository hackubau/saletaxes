package it.hackubau.saleTaxes.db.repositories;


import it.hackubau.saleTaxes.db.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository("productsRepository")
@Transactional(propagation = Propagation.SUPPORTS)
public interface ProductsRepository extends JpaRepository<Product, Long> {

    Optional<Product> findById(Long id);

}
