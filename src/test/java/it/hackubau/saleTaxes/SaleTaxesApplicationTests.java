package it.hackubau.saleTaxes;

import it.hackubau.saleTaxes.db.entities.Product;
import it.hackubau.saleTaxes.db.entities.Tax;
import it.hackubau.saleTaxes.enums.ProductType;
import it.hackubau.saleTaxes.services.ProductService;
import it.hackubau.saleTaxes.services.TaxesService;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.assertj.core.util.Lists;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SaleTaxesApplicationTests {

    private static final Logger logger = LoggerFactory.getLogger(SaleTaxesApplicationTests.class);

    @Autowired
    ProductService productService;

    @Autowired
    TaxesService taxesService;

    /**
     * PRODUCTS TEST CASES
     */

    private final List<Product> productsTest1 = Lists.newArrayList(
            Product.builder().name("book").productType(ProductType.BOOKS).price(new BigDecimal("12.49")).imported(false).build(),
            Product.builder().name("music CD").productType(ProductType.GENERIC).price(new BigDecimal("14.99")).imported(false).build(),
            Product.builder().name("chocolate bar").productType(ProductType.FOODS).price(new BigDecimal("0.85")).imported(false).build()
    );

    private final List<Product> productsTest2 = Lists.newArrayList(
            Product.builder().name("imported box of chocolates").productType(ProductType.FOODS).price(new BigDecimal("10.00")).imported(true).build(),
            Product.builder().name("imported bottle of perfume").productType(ProductType.GENERIC).price(new BigDecimal("47.50")).imported(true).build()
    );

    private final List<Product> productsTest3 = Lists.newArrayList(
            Product.builder().name("imported bottle of perfume").productType(ProductType.GENERIC).price(new BigDecimal("27.99")).imported(true).build(),
            Product.builder().name("bottle of perfume").productType(ProductType.GENERIC).price(new BigDecimal("18.99")).imported(false).build(),
            Product.builder().name("packet headache pills").productType(ProductType.MEDICAL).price(new BigDecimal("9.75")).imported(false).build(),
            Product.builder().name("box of imported chocolates").productType(ProductType.FOODS).price(new BigDecimal("11.25")).imported(true).build()

    );

    /**
     * PRODUCTS TYPES TAXES
     */

    private final List<Tax> taxesGenerics = Lists.newArrayList(
            Tax.builder().productType(ProductType.GENERIC).standardPerc(new BigDecimal(10)).build(),
            Tax.builder().productType(ProductType.FOODS).standardPerc(new BigDecimal(0)).build(),
            Tax.builder().productType(ProductType.BOOKS).standardPerc(new BigDecimal(0)).build(),
            Tax.builder().productType(ProductType.MEDICAL).standardPerc(new BigDecimal(0)).build());


    /**
     * SETUP DATABASE OBJECTS
     */

    @Transactional(propagation = Propagation.SUPPORTS)
    public List<Tax> setUpTaxes(List<Tax> taxes) {
        return taxesService.save(taxes);
    }

    @Transactional(propagation = Propagation.SUPPORTS)
    public Map<Product, Long> setUpProducts(List<Product> products) {

        Map<Product, Long> shopCart = new LinkedHashMap<>();
        products.forEach(s -> shopCart.merge(productService.save(s), 1L, Math::addExact));
        return shopCart;

    }

    /**
     * DO CALCULATIONS
     */

    @Transactional(propagation = Propagation.SUPPORTS)
    public List<TestOutputObj> calculateTaxes(List<Tax> taxes, List<Product> products) {

        setUpTaxes(taxes);
        Map<Product, Long> productList = setUpProducts(products);

        return productList.entrySet().stream().map(x ->
                TestOutputObj.builder()
                        .product(x.getKey())
                        .quantity(x.getValue().intValue())
                        .priceAfterTax(taxesService.getTaxedPrice(x.getKey()))
                        .taxAmountPerPiece(taxesService.getTaxAmount(x.getKey())).build()
        ).collect(Collectors.toList());
    }

    /**
     * TEST CALL
     */

    @Test
    @Transactional(propagation = Propagation.SUPPORTS)
    public void t01() {
        List<TestOutputObj> out = calculateTaxes(taxesGenerics, productsTest1);
        checkResult(new BigDecimal("1.50"), new BigDecimal("29.83"), out);
    }

    @Test
    public void t02() {

        List<TestOutputObj> out = calculateTaxes(taxesGenerics, productsTest2);
        checkResult(new BigDecimal("7.65"), new BigDecimal("65.15"), out);
    }

    @Test
    public void t03() {

        List<TestOutputObj> out = calculateTaxes(taxesGenerics, productsTest3);
        checkResult(new BigDecimal("6.70"), new BigDecimal("74.68"), out);
    }

    /**
     * RESULT CHECK HELPER
     */

    @Transactional(propagation = Propagation.SUPPORTS)
    public void checkResult(BigDecimal salesTaxes, BigDecimal total, List<TestOutputObj> testOutputs) {

        BigDecimal totalPrice = new BigDecimal("0");
        BigDecimal saleTaxes = new BigDecimal("0");

        for (TestOutputObj to : testOutputs) {
            BigDecimal realPrice = to.getPriceAfterTax().multiply(new BigDecimal(to.getQuantity()));
            totalPrice = totalPrice.add(realPrice);
            saleTaxes = saleTaxes.add(to.getTaxAmountPerPiece().multiply(new BigDecimal(to.getQuantity())));
            logger.info(to.quantity + " " + to.getProduct().getName() + " - " + realPrice);
        }
        logger.info("Sale Taxes: " + saleTaxes);
        logger.info("Total: " + totalPrice);
        Assert.assertEquals(saleTaxes.compareTo(salesTaxes), 0);
        Assert.assertEquals(totalPrice.compareTo(total), 0);

    }


}

/**
 * TEST OBJECT HELPER
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
class TestOutputObj {
    Product product;
    Integer quantity;
    BigDecimal taxAmountPerPiece;
    BigDecimal priceAfterTax;

}

