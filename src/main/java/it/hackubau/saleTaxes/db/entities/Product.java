package it.hackubau.saleTaxes.db.entities;


import it.hackubau.saleTaxes.enums.ProductType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "Products")
@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class Product {

    @Id
    @SequenceGenerator(name = "SEQ_idProduct", sequenceName = "SEQ_idProduct")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_idProduct")
    private Long id;

    private String name;

    private String description;

    private BigDecimal price;

    @Column(name = "product_type")
    private ProductType productType;

    @Column(columnDefinition = "bit default 0")
    private boolean imported;

}
