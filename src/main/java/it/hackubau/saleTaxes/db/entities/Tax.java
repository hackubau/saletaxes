package it.hackubau.saleTaxes.db.entities;


import it.hackubau.saleTaxes.enums.ProductType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "Taxes")
@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class Tax {

    @Id
    private ProductType productType;

    private BigDecimal standardPerc;

}
