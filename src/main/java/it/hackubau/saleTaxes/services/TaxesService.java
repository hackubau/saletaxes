package it.hackubau.saleTaxes.services;


import it.hackubau.saleTaxes.db.entities.Product;
import it.hackubau.saleTaxes.db.entities.Tax;
import it.hackubau.saleTaxes.db.repositories.TaxesRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS)
public class TaxesService {

    @Value("${products.tax.importedModifier}")
    BigDecimal importedModifier;

    private static final Logger logger = LoggerFactory.getLogger(TaxesService.class);

    final TaxesRepository taxesRepository;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public List<Tax> save(List<Tax> p) {
        return taxesRepository.saveAll(p);
    }


    /**
     *
     * @param p product
     * @return Tax percentual for the product
     */
    private BigDecimal getTaxPerc(Product p) {
        return p != null ?
                getTaxPerc(p, taxesRepository.findById(p.getProductType())
                        .orElse(Tax.builder().standardPerc(BigDecimal.ZERO).build())) : BigDecimal.ZERO;
    }

    /**
     *
     * @param p product
     * @param t tax object
     * @return the taxt percentual for the product
     */
    private BigDecimal getTaxPerc(Product p, Tax t) {
        try {

            return p.isImported() ?
                    t.getStandardPerc().add(importedModifier) : t.getStandardPerc();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return BigDecimal.ZERO;
        }
    }

    /**
     *
     * @param p the product
     * @return the price included taxes
     */
    public BigDecimal getTaxedPrice(Product p) {
        return p != null ? getTaxedPrice(p.getPrice(), getTaxPerc(p)) : BigDecimal.ZERO;
    }


    /**
     *
     * @param productPrice the product price
     * @param taxPerc the tax percentual
     * @return the price included taxes
     */
    public BigDecimal getTaxedPrice(BigDecimal productPrice, BigDecimal taxPerc) {
        try {
            return applyTaxAmount(productPrice, getTaxAmount(productPrice, taxPerc));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return BigDecimal.ZERO;
        }
    }


    /**
     *
     * @param p product
     * @return the tax amount to add (or subtract) to the product price
     */
    public BigDecimal getTaxAmount(Product p) {
        return p != null ? getTaxAmount(p.getPrice(), getTaxPerc(p)) : BigDecimal.ZERO;
    }

    /**
     *
     * @param prodPrice product price
     * @param taxPerc tax precentual
     * @return the tax amount to add (or subtract) to the product price
     */
    public BigDecimal getTaxAmount(BigDecimal prodPrice, BigDecimal taxPerc) {
        try {

            return taxAmountRound(taxPerc.multiply(prodPrice).divide(new BigDecimal(100)));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return BigDecimal.ZERO;
        }
    }


    /**
     *
     * @param productPrice the product price
     * @param taxAmount the tax amount
     * @return the sum between product price and tax amount
     */

    private BigDecimal applyTaxAmount(BigDecimal productPrice, BigDecimal taxAmount) {
        try {
            return productPrice.add(taxAmount);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return BigDecimal.ZERO;
        }
    }

    /**
     *
     * @param amount the amount
     * @return the round for the amount to the nearest 0.05 rounded up
     */

    private BigDecimal taxAmountRound(BigDecimal amount) {

        BigDecimal incrementoInvertito = new BigDecimal("20");

        BigDecimal divided = amount.multiply(incrementoInvertito)
                .setScale(0, BigDecimal.ROUND_UP);


        return divided.divide(incrementoInvertito)
                .setScale(2, BigDecimal.ROUND_UP);
    }

}
