package it.hackubau.saleTaxes.enums;


public enum ProductType {
    GENERIC,
    BOOKS,
    FOODS,
    MEDICAL
}
