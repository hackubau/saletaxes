package it.hackubau.saleTaxes.services;


import it.hackubau.saleTaxes.db.entities.Product;
import it.hackubau.saleTaxes.db.repositories.ProductsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class ProductService {


    final ProductsRepository productsRepository;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Product save(Product p) {
        return productsRepository.save(p);
    }


}
