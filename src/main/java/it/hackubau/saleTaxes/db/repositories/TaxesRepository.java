package it.hackubau.saleTaxes.db.repositories;


import it.hackubau.saleTaxes.db.entities.Tax;
import it.hackubau.saleTaxes.enums.ProductType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository("taxesRepository")
@Transactional(propagation = Propagation.SUPPORTS)
public interface TaxesRepository extends JpaRepository<Tax, ProductType> {
}
